table = [
    ['green', 'green', 'blue', 'red'],
    ['green', 'yellow', 'blue', 'red'],
    ['green', 'yellow', 'yellow', 'blue'],
]


def line_processing(line):
    result = {x: 0 for x in set(line)}
    current = {x: 0 for x in set(line)}

    prev = None
    for item in line:
        if prev == item:
            current[item] += 1
        else:
            prev = item
            current[item] = 1
        result[item] = max(result[item], current[item])

    return result


def all_lines_processing(the_table):
    lines_result = {}
    for row in the_table:
        for key, value in line_processing(row).items():
            if key in lines_result:
                lines_result[key] = max(lines_result[key], value)
            else:
                lines_result[key] = value
    return lines_result


def main(the_table):
    by_rows = all_lines_processing(the_table)

    table_columns = list(zip(*the_table))
    by_columns = all_lines_processing(table_columns)

    by_rows_and_columns = [*by_rows.items(), *by_columns.items()]

    the_max = max((v for k, v in by_rows_and_columns))

    return {k: v for k, v in by_rows_and_columns if v == the_max}


if __name__ == '__main__':
    # самых длинных может быть несколько,
    # что делать в этом случае в задании не сказано, так что пока выведем все
    print(main(table))
