table = [
    ['green', 'green', 'blue', 'red', 'yellow'],
    ['green', 'yellow', 'blue', 'red', 'blue'],
    ['green', 'yellow', 'yellow', 'blue', 'blue', 'blue', 'yellow'],
]


# =============================================
# Улучшение 1.0 (начать - пол кода переделать)

# сначала взялся за all_lines_processing
# сосредотачиваемся на задаче обработать пачку, а максимум позже найдём
def all_lines_processing(the_table):
    res = []
    for line in the_table:
        res.extend(line_processing(line))
    return res

# небольшие правки line_processing не давали избавиться от эффекта Pascal'я
# пришлось вспомнить о groupby
from itertools import groupby


def line_processing(line):
    return [(c, len(list(i))) for c, i in groupby(line)]


# добрались до поиска максимального значения
def get_max_colors(list_of_tuples):
    maximum = max(list_of_tuples, key=lambda x: x[1])
    return [i for i in list_of_tuples if i[1] == maximum[1]]


# ну вот и стало более питонично!
def main(the_table):
    by_rows = all_lines_processing(the_table)
    by_columns = all_lines_processing(list(zip(*the_table)))
    return get_max_colors([*by_rows, *by_columns])


# ====================================================
# Улучшение 2.0 (однострочники порождают зависимость)

# all_lines_processing хочется превратить в однострочник..
def all_lines_processing(the_table):
    return [line_processing(line) for line in (*the_table, *zip(*the_table))]
# Но в таком варианте будет падать get_max_colors из-за вложенных списков


# Тут бы задуматься и остановиться.. Но мысль о генераторах начинает играть новыми красками, и..


# И действиетльно, перевести на генераторы line_processing не трудно:
def line_processing(line):
    return ((c, len(list(i))) for c, i in groupby(line))


# Следом за ней изящнее становится и all_lines_processing
def all_lines_processing(the_table):
    return (line_processing(line) for line in (*the_table, *zip(*the_table)))


# В итоге... В итоге приходится тратить изрядное количество времени на отладку!
# Лезть в модуль itertools в поисках chain..

from itertools import chain


def main(the_table):
    tmp = list(chain.from_iterable(all_lines_processing(the_table)))
    return list(get_max_colors(tmp))


if __name__ == '__main__':
    # самых длинных может быть несколько,
    # что делать в этом случае в задании не сказано, так что пока выведем все
    print(main(table))
