table = [
    ['green', 'green', 'blue', 'red', 'yellow'],
    ['green', 'yellow', 'blue', 'red', 'blue'],
    ['green', 'yellow', 'yellow', 'blue', 'blue', 'blue', 'yellow'],
]

from itertools import groupby, tee, chain


# ===========================================
# возвращаем первую из самых длинных цепочек

def very_short_decision(t):
    tmp = chain.from_iterable((((c, len(list(i))) for c, i in groupby(l)) for l in (*t, *zip(*t))))
    return max(tmp, key=lambda x: x[1])


def one_line_decision(t):
    return max(chain.from_iterable((((c, len(list(i))) for c, i in groupby(l)) for l in (*t, *zip(*t)))), key=lambda x: x[1])


# ===========================================
# возвращаем все цепочки максимальной длины

def short_decision(t):
    tmp = chain.from_iterable((((c, len(list(i))) for c, i in groupby(l)) for l in (*t, *zip(*t))))
    first, second = tee(tmp, 2)
    maximum = max(first, key=lambda x: x[1])
    return list(i for i in second if i[1] == maximum[1])


# -------------------------------------------
# Более читаемое решение выглядит так

def readable_decision(the_table):
    color_and_length = chain.from_iterable(
        (
            ((color, len(list(item_iter))) for color, item_iter in groupby(line))
            for line in (*the_table, *zip(*the_table))
        )
    )
    first, second = tee(color_and_length, 2)
    maximum_length = max(first, key=lambda x: x[1])[1]
    return list(i for i in second if i[1] == maximum_length)


# -------------------------------------------
# Заменим len(list(item_iter)) на sum((1 for _ in x)) чтобы не создавать список в памяти
# Явно выпишем полезное преобразование таблицы в набор строк и транспонированных столбцов

def decision_tee(the_table):
    all_lines = (*the_table, *zip(*the_table))
    amount = lambda x: sum((1 for _ in x))  # и да, я знаю что PEP8 будет недоволен

    color_and_length = chain.from_iterable(
        (((color, amount(item_iter)) for color, item_iter in groupby(line)) for line in all_lines)
    )

    first, second = tee(color_and_length, 2)
    maximum_length = max(first, key=lambda x: x[1])[1]
    return list(i for i in second if i[1] == maximum_length)


# И наконец, лучшее решение! То которое проще всего поддерживать:
#   три небольшие функции, размещённые в одном файле!
# Хотите версию на списках - пожалуйста!
# Решили перейти на генераторы - обложите тестами отдельные функции и совершайте переход без боязни.

def best_decision(the_table):

    def line_processing(line):
        return [(color, len(list(item_iter))) for color, item_iter in groupby(line)]

    def get_max_colors(list_of_tuples):
        maximum = max(list_of_tuples, key=lambda x: x[1])
        return [i for i in list_of_tuples if i[1] == maximum[1]]

    def main(the_table):
        all_lines = (*the_table, *zip(*the_table))
        tmp = []
        for line in all_lines:
            tmp.extend(line_processing(line))
        return get_max_colors(tmp)

    return main(the_table)


def best_decision_generators(the_table):

    def line_processing(line):
        return ((color, sum(1 for _ in item_iter)) for color, item_iter in groupby(line))

    def get_max_colors(iterable):
        first, second = tee(iterable, 2)
        maximum = max(first, key=lambda x: x[1])
        return (i for i in second if i[1] == maximum[1])

    def main(the_table):
        all_lines = (*the_table, *zip(*the_table))
        tmp = chain.from_iterable((line_processing(line) for line in all_lines))
        return list(get_max_colors(tmp))

    return main(the_table)


# ===========================================
# В качестве домашнего задания: доработайте нижеприведённое решение НА ГЕНЕРАТОРАХ,
#   которое бы не использовало tee (ну не знаете вы про tee на момент собеседования)
#   и при этом выводило бы ВСЕ цепочки максимальной длины (сейчас выводит только одну).

def decision_without_tee(the_table):
    all_lines = (*the_table, *zip(*the_table))
    amount = lambda x: sum((1 for _ in x))

    color_and_length = chain.from_iterable(
        (((color, amount(item_iter)) for color, item_iter in groupby(line)) for line in all_lines)
    )

    result = ((length, list(item_iter)) for length, item_iter in groupby(color_and_length, key=lambda x: x[1]))
    # Подсказка: видно, что информация о ('green', 3) не потеряна, но структура данных оставляет желать..
    # print(list(result))
    return max(result, key=lambda x: x[0])[1]


if __name__ == '__main__':

    variants = [one_line_decision, very_short_decision,
                short_decision, readable_decision, decision_tee, best_decision, best_decision_generators,
                decision_without_tee]

    for func in variants:
        # if func.__name__ != 'decision_without_tee': continue  # Это для желающих повозиться с decision_without_tee =]
        print(f'{func.__name__} returns:', func(table))
