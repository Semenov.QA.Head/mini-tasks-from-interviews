import cProfile, pstats, io
from functools import wraps


def profile(func):
    @wraps(func)
    def wrapper(*args, **kwargs):

        pro = cProfile.Profile()
        pro.enable()

        try:
            result = func(*args, **kwargs)
        except AssertionError as e:
            result = None
            print('AssertionError:', e)

        pro.disable()
        stream = io.StringIO()

        stat = pstats.Stats(pro, stream=stream)
        stat.strip_dirs().sort_stats('cumtime').print_stats(20)
        print(stream.getvalue())

        return result
    return wrapper
