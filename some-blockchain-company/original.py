import random


def fun():
    result = []
    x = list(range(51))
    for i in range(100):
        result.append(random.choice(x))
    return result


def only_three(data):
    return [x for x in data if not (x % 3)]


def test_fun_len():
    assert len(fun()) == 100


def test_fun_max_value():
    assert all((x <= 50 for x in fun()))


def test_fun_returns_different_results():
    assert fun() != fun()


def test_fun_really_use_random():
    eps = 0.025

    batch_size = 51
    batch_sum = sum(range(batch_size))
    n = 5_000

    n_count = n * batch_size
    expected_average = n * batch_sum

    norm = lambda x: abs(sum(x) - expected_average) / expected_average

    tmp = [fun() for _ in range(n_count)]
    result = [norm(i) for i in zip(*tmp)]

    assert all((i < eps for i in result))


def main():
    test_fun_len()
    test_fun_max_value()
    test_fun_returns_different_results()
    test_fun_really_use_random()


if __name__ == '__main__':
    main()
