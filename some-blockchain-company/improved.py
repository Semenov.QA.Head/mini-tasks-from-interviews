import concurrent.futures

from original import fun
from helper import profile


@profile
def test_fun_really_use_random():
    eps = 0.003

    batch_size = 51
    batch_sum = sum(range(batch_size))
    n = 10_000

    n_count = n * batch_size
    expected_average = n * batch_sum

    norm = lambda x: abs(sum(x) - expected_average) / expected_average

    # tmp = [fun() for _ in range(n_count)]

    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        futures = [executor.submit(fun) for _ in range(n_count)]
        tmp = [item.result() for item in concurrent.futures.as_completed(futures)]

    result = [norm(i) for i in zip(*tmp)]

    delta = max(result)
    assert delta <= eps, f'delta > eps! delta = {delta} eps = {eps}'


if __name__ == '__main__':
    test_fun_really_use_random()
