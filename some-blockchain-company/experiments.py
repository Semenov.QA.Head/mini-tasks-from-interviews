import random
import concurrent.futures

from helper import profile


eps = 0.001
n = 5_000
batch_size = 51
batch_sum = sum(range(batch_size))
n_count = n * batch_size
expected_average = n * batch_sum
norm = lambda x: abs(sum(x) - expected_average) / expected_average

WORKERS_COUNT = 4


def very_fun(seed=None):
    rand = random.Random(seed)
    result = []
    x = list(range(51))
    for i in range(100):
        result.append(rand.choice(x))
    return result


def routine_v1():
    tmp = [very_fun() for _ in range(n_count)]
    return [norm(i) for i in zip(*tmp)]


def routine_v2():
    with concurrent.futures.ThreadPoolExecutor(max_workers=WORKERS_COUNT) as executor:
        futures = [executor.submit(very_fun) for _ in range(n_count)]
        tmp = [item.result() for item in concurrent.futures.as_completed(futures)]
    return [norm(i) for i in zip(*tmp)]


def routine_v3():
    from itertools import cycle
    go = cycle(range(WORKERS_COUNT))

    with concurrent.futures.ThreadPoolExecutor(max_workers=WORKERS_COUNT) as executor:
        futures = []
        for _ in range(n_count):
            futures.append(executor.submit(very_fun, next(go)))
        tmp = [item.result() for item in concurrent.futures.as_completed(futures)]

    return [norm(i) for i in zip(*tmp)]


@profile
def test_very_fun_v1():
    delta = max(routine_v1())
    assert delta <= eps, f'delta > eps! delta = {delta} eps = {eps}'

@profile
def test_very_fun_v2():
    delta = max(routine_v2())
    assert delta <= eps, f'delta > eps! delta = {delta} eps = {eps}'

@profile
def test_very_fun_v3():
    delta = max(routine_v3())
    assert delta <= eps, f'delta > eps! delta = {delta} eps = {eps}'


if __name__ == '__main__':
    import sys; print(sys.version)
    for test in [test_very_fun_v1, test_very_fun_v2, test_very_fun_v3]:
        print(test.__name__)
        test()


""" The output will be something like this:

3.7.8 (default, Jul 30 2020, 13:03:23) 
[Clang 11.0.0 (clang-1100.0.33.8)]
test_very_fun_v1
AssertionError: delta > eps! delta = 0.0034175686274509802 eps = 0.001
         160769286 function calls in 78.261 seconds

   Ordered by: cumulative time
   List reduced from 21 to 20 due to restriction <20>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.103    0.103   78.261   78.261 experiments.py:52(test_very_fun_v1)
        1    0.587    0.587   78.158   78.158 experiments.py:27(routine_v1)
        1    0.136    0.136   75.461   75.461 experiments.py:28(<listcomp>)
   255000   24.519    0.000   75.325    0.000 experiments.py:18(very_fun)
 25500000    9.904    0.000   30.764    0.000 random.py:256(choice)
 25500000   12.567    0.000   18.699    0.000 random.py:224(_randbelow)
   255000    0.110    0.000   17.707    0.000 random.py:88(__init__)
   255000    0.705    0.000   17.598    0.000 random.py:97(seed)
   255000   16.813    0.000   16.813    0.000 {function Random.seed at 0x104ed3e60}
 31993979    4.105    0.000    4.105    0.000 {method 'getrandbits' of '_random.Random' objects}
 25500000    2.335    0.000    2.335    0.000 {method 'append' of 'list' objects}
 25500000    2.161    0.000    2.161    0.000 {built-in method builtins.len}
        1    1.801    1.801    2.110    2.110 experiments.py:29(<listcomp>)
 25500000    2.027    0.000    2.027    0.000 {method 'bit_length' of 'int' objects}
      100    0.000    0.000    0.310    0.003 experiments.py:13(<lambda>)
      100    0.309    0.003    0.309    0.003 {built-in method builtins.sum}
   255000    0.079    0.000    0.079    0.000 {built-in method builtins.isinstance}
      100    0.000    0.000    0.000    0.000 {built-in method builtins.abs}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.max}



test_very_fun_v2
AssertionError: delta > eps! delta = 0.0026986666666666665 eps = 0.001
         12112996 function calls (12112985 primitive calls) in 71.608 seconds

   Ordered by: cumulative time
   List reduced from 174 to 20 due to restriction <20>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.403    0.403   71.608   71.608 experiments.py:57(test_very_fun_v2)
        1    1.022    1.022   71.206   71.206 experiments.py:32(routine_v2)
        1    0.148    0.148   60.653   60.653 experiments.py:35(<listcomp>)
   255001    0.769    0.000   59.876    0.000 _base.py:196(as_completed)
   216171    0.401    0.000   57.074    0.000 threading.py:534(wait)
   216170    0.689    0.000   56.451    0.000 threading.py:264(wait)
   864684   55.390    0.000   55.390    0.000 {method 'acquire' of '_thread.lock' objects}
        1    5.645    5.645    6.068    6.068 experiments.py:36(<listcomp>)
        1    0.110    0.110    3.462    3.462 experiments.py:34(<listcomp>)
   255000    0.907    0.000    3.351    0.000 thread.py:145(submit)
   255000    0.890    0.000    2.098    0.000 _base.py:312(__init__)
   255005    1.047    0.000    1.208    0.000 threading.py:216(__init__)
   471168    0.568    0.000    1.091    0.000 _base.py:174(_yield_finished_futures)
   255000    0.346    0.000    0.629    0.000 _base.py:408(result)
   942338    0.289    0.000    0.537    0.000 threading.py:240(__enter__)
      100    0.001    0.000    0.423    0.004 experiments.py:13(<lambda>)
      100    0.423    0.004    0.423    0.004 {built-in method builtins.sum}
   216167    0.205    0.000    0.396    0.000 threading.py:524(clear)
   942338    0.271    0.000    0.392    0.000 threading.py:243(__exit__)
   216170    0.094    0.000    0.168    0.000 threading.py:252(_acquire_restore)



test_very_fun_v3
AssertionError: delta > eps! delta = 0.74 eps = 0.001
         8383049 function calls in 37.278 seconds

   Ordered by: cumulative time
   List reduced from 79 to 20 due to restriction <20>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.459    0.459   37.278   37.278 experiments.py:62(test_very_fun_v3)
        1    0.988    0.988   36.819   36.819 experiments.py:39(routine_v3)
        1    0.095    0.095   18.389   18.389 experiments.py:47(<listcomp>)
   255001   12.785    0.000   17.790    0.000 _base.py:196(as_completed)
   255000    3.371    0.000   11.856    0.000 thread.py:145(submit)
   255000    1.416    0.000    5.838    0.000 _base.py:312(__init__)
        1    5.122    5.122    5.541    5.541 experiments.py:49(<listcomp>)
   255005    3.549    0.000    4.422    0.000 threading.py:216(__init__)
    31854    0.046    0.000    3.520    0.000 threading.py:534(wait)
    31853    0.080    0.000    3.444    0.000 threading.py:264(wait)
   127416    3.317    0.000    3.317    0.000 {method 'acquire' of '_thread.lock' objects}
   255000    1.610    0.000    1.610    0.000 thread.py:46(__init__)
   255000    0.957    0.000    0.982    0.000 thread.py:176(_adjust_thread_count)
   255000    0.873    0.000    0.873    0.000 threading.py:75(RLock)
   286851    0.377    0.000    0.746    0.000 _base.py:174(_yield_finished_futures)
   255000    0.259    0.000    0.504    0.000 _base.py:408(result)
      100    0.001    0.000    0.419    0.004 experiments.py:13(<lambda>)
      100    0.418    0.004    0.418    0.004 {built-in method builtins.sum}
   573704    0.161    0.000    0.320    0.000 threading.py:240(__enter__)
        1    0.156    0.156    0.219    0.219 _base.py:144(__enter__)
"""
